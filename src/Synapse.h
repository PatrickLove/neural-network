#ifndef __SYNAPSE_H
#define __SYNAPSE_H

#include <cstddef>

namespace nnet {
	class Neuron;

	class Synapse {
	private:
		double weight;

		double activationState;

		Neuron* source;
		Neuron* target;
		double dEdW;
		double dEdIn;

	public:

		Synapse() : weight(1), source(NULL), target(NULL) {}

		void linkSource(Neuron* s) { source = s; }
		void linkTarget(Neuron& t);

		void setWeight(double w) { weight = w; }
		double getWeight() const { return weight; }
		double getdEdIn() const { return dEdIn; }
		double getActivation() const { return activationState; }

		void feedForward(double activation);

		void computeDerivatives();

		void performGradientDescent(double learnRate);
	};
}

#endif