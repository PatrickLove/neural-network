#include "NeuralNetwork.h"
#include <sstream>
#include <fstream>
#include <string>
#include <cstring>
#include "Activation/SoftSignActivator.h"
#include "Activation/LinearActivator.h"
#include "NetworkTrainer.h"
#include <vector>
#include <cstdlib>
#include <ctime>



void printVectBrackets(const nnet::NetworkInput& vect) {
	std::cout << "{ ";
	if(vect.size()>0){
		std::cout << vect[0];
		for(int i = 1; i < vect.size(); ++i){
			std::cout << ", " << vect[i];
		}
	}
	std::cout << " }";
}

int main() {
	srand(time(NULL));
	unsigned int bLen = sizeof(char)+4*sizeof(unsigned int)+9*sizeof(double);
	char* buffer = new char[bLen];
	*buffer = LINEAR_ACTIVATOR_TYPE;
	unsigned int* intPos = (unsigned int*)(buffer+1);
	*((double*)intPos) = 1.0;
	intPos += sizeof(double)/sizeof(unsigned int);
	*(intPos++) = 3;
	*(intPos++) = 1;
	*(intPos++) = 2;
	*(intPos++) = 1;
	double* weightArr = (double*)intPos;
	*(weightArr++) = 0.0;
	*(weightArr++) = 0.5;
	*(weightArr++) = 1/3.0;
	*(weightArr++) = 0.0;
	*(weightArr++) = 1.0;
	*(weightArr++) = 0.0;
	*(weightArr++) = 1.5;
	*(weightArr++) = 0.0;

	std::istringstream stream(std::string(buffer, bLen));

	nnet::NeuralNetwork network = nnet::NeuralNetwork::deserializeFromStream(stream);

	std::cout << "f(1) = " << network.eval(std::vector<double>(1,1.0))[0] << std::endl;
	std::cout << "f(2) = " << network.eval(std::vector<double>(1,2.0))[0] << std::endl;

	std::stringstream outStr;
	network.serializeToStream(outStr);
	std::string outStrObj = outStr.str();
	std::cout << "Equal Serialization: " << !memcmp(outStrObj.c_str(), buffer, bLen) << std::endl;

	unsigned int layerCounts[3];
	layerCounts[0] = 4;
	layerCounts[1] = 10;
	layerCounts[2] = 3;
	nnet::SoftSignActivator activator;
	nnet::NeuralNetwork net2(3, layerCounts, &activator);

	net2.initRand(-1.0, 1.0);

	nnet::TrainingSet trainingSamples;
	// trainingSamples.push_back(nnet::TrainingItem({1.0, 1.0}, {-1.0}));
	// trainingSamples.push_back(nnet::TrainingItem({1.0, -1.0}, {1.0}));
	// trainingSamples.push_back(nnet::TrainingItem({-1.0, 1.0}, {1.0}));
	// trainingSamples.push_back(nnet::TrainingItem({-1.0, -1.0}, {-1.0}));
	std::ifstream datafile("iris.data");
	double in;
	std::string iris;
	nnet::NetworkInput ouput = {-1,-1,-1};
	while(datafile) {
		nnet::NetworkInput input;
		input.reserve(4);
		for(int i = 0; i < 4; ++i){
			datafile >> in;
			input.push_back(in);
		}
		datafile >> iris;
		nnet::NetworkInput localOut(ouput);
		if(iris == "Iris-setosa"){
			localOut[0] = 1.0;
		} else if (iris == "Iris-versicolor") {
			localOut[1] = 1.0;
		} else if (iris == "Iris-virginica") {
			localOut[2] = 1.0;
		}
		trainingSamples.push_back(nnet::TrainingItem(input, localOut));
	}

	nnet::NetworkTrainer trainer(trainingSamples, 0.2);

	for (int s = 0; s < 10000; ++s)
	{
		trainer.trainNetworkOnce(net2);
	}

	for(unsigned int i = 0 ; i < trainingSamples.size(); ++i){
		printVectBrackets(net2.eval(trainingSamples[i].input));
		std::cout << " ~ ";
		printVectBrackets(trainingSamples[i].expected);
		std::cout << std::endl;
	}
}