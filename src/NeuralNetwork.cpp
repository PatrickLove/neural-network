#include "NeuralNetwork.h"
#include <algorithm>

//Useful looping macros
#define FOR_ALL_LAYERS(l) FOR_LAYERS_RANGE(l, 0, 0)
#define FOR_LAYERS_END(l, end) FOR_LAYERS_RANGE(l, 0, end)
#define FOR_LAYERS_RANGE(l, st, end) for(uint l = st; l < layerCount+end; ++l)

#define FOR_NEURONS(layer, n) for(uint n = 0; n < layerSizes[layer]; ++n)
#define FOR_INPUT_NEURONS(n) FOR_NEURONS(0, n)
#define FOR_OUTPUT_NEURONS(n) FOR_NEURONS(layerCount-1, n)

typedef unsigned int uint;

namespace nnet {

	NeuralNetwork::NeuralNetwork(uint layers, uint* layerSizeArr, Activator* func) : activeFunc(func->clone()), layerCount(layers) {
		layerSizes = new uint[layerCount];
		std::copy(layerSizeArr, layerSizeArr+layerCount, layerSizes);
		allocNetwork();
		initAndLink();
	}
	NeuralNetwork::NeuralNetwork(const NeuralNetwork& other) : layerCount(other.layerCount), activeFunc(other.activeFunc->clone()) {
		layerSizes = new uint[layerCount];
		std::copy(other.layerSizes, other.layerSizes+layerCount, layerSizes);
		allocNetwork();
		initAndLink();
		FOR_LAYERS_END(i,-1) {
			FOR_NEURONS(i, j) {
				Synapse* uninit = network[i][j].getSynapses();
				const Synapse* init = other.network[i][j].getSynapses();
				for(unsigned int k = 0; k < layerSizes[i+1]; ++k){
					uninit[k].setWeight(init[k].getWeight());
				}
			}
		}
	}
	NeuralNetwork& NeuralNetwork::operator= (const NeuralNetwork& other) {
		if(this != &other){
			NeuralNetwork tmp(other);
			std::swap(network, tmp.network);
			std::swap(activeFunc, tmp.activeFunc);
			std::swap(layerSizes, tmp.layerSizes);
			std::swap(layerCount, tmp.layerCount);
		}
		return *this;
	}
	NeuralNetwork::~NeuralNetwork() {
		for (unsigned int i = 0; i < layerCount; ++i) {
			delete [] network[i];
		}
		delete [] network;
		delete activeFunc;
		delete [] layerSizes;
	}

	void NeuralNetwork::initAndLink() {
		//Iterate backwards since linking must be done to initialized neurons
		for(uint l = layerCount-1; l < layerCount; --l) {
			FOR_NEURONS(l, n) {
				uint lastSize = l>0 ? layerSizes[l-1] : 0;
				uint nextSize = l!=outputLayer() ? layerSizes[l+1] : 0;

				network[l][n].init(activeFunc, lastSize, nextSize);

				Synapse* outputs = network[l][n].getSynapses();
				for (uint s = 0; s < nextSize; ++s){
					outputs[s].linkTarget(network[l+1][s]);
				}
			}
		}
	}

	void NeuralNetwork::allocNetwork() {
		network = new Neuron*[layerCount];
		for (unsigned int i = 0; i < layerCount; ++i) {
			network[i] = new Neuron[layerSizes[i]];
		}
	}

	NetworkInput NeuralNetwork::eval(const NetworkInput& inputs){
		FOR_INPUT_NEURONS(i) {
			network[0][i].activate(inputs[i]);
		}
		FOR_ALL_LAYERS(l) {
			FOR_NEURONS(l, n) {
				network[l][n].feedForward();
			}
		}
		std::vector<double> ret;
		ret.reserve(outputCount());
		FOR_OUTPUT_NEURONS(i) {
			ret.push_back(network[outputLayer()][i].getOutput());
		}
		return ret;
	}

	void NeuralNetwork::train(const NetworkInput& expectedOutputs, double learnRate) {
		FOR_OUTPUT_NEURONS(i) {
			network[outputLayer()][i].computeError(expectedOutputs[i]);
		}
		FOR_ALL_LAYERS(l) {
			FOR_NEURONS(outputLayer()-l, n) {
				network[outputLayer()-l][n].computeDerivatives();
			}
		}
		FOR_ALL_LAYERS(l) {
			FOR_NEURONS(l, n) {
				network[l][n].trainOutputWeights(learnRate);
			}
		}
	}

	uint NeuralNetwork::getTotalWeights() {
		uint ret = 0;
		FOR_ALL_LAYERS(i) {
			ret += layerSizes[i]; //Biases
			if(i != layerCount-1){
				ret += layerSizes[i] * layerSizes[i+1]; //Synapses
			}
		}
		return ret;
	}

	double genRandomWeight(double min, double max) {
		double r = ((double)rand()/RAND_MAX);
		return min + r * (max-min);
	}

	void NeuralNetwork::initRand(double min, double max) {
		FOR_LAYERS_END(i,-1) {
			FOR_NEURONS(i, j) {
				network[i][j].setBias(genRandomWeight(min, max));
				Synapse* synapses = network[i][j].getSynapses();
				for(unsigned int k = 0; k < layerSizes[i+1]; ++k){
					synapses[k].setWeight(genRandomWeight(min, max));
				}
			}
		}
	}

	void NeuralNetwork::serializeToStream(std::ostream& out) {
		activeFunc->serialize(out);
		out.write((char*)&layerCount, sizeof(uint));
		out.write((char*)layerSizes, sizeof(uint)*layerCount);

		uint weights = getTotalWeights();
		double* netBuffer = new double[weights];
		double* pos = netBuffer;
		FOR_ALL_LAYERS(i) {
			FOR_NEURONS(i, j) {
				*(pos++) = network[i][j].getBias();
				if(i != layerCount-1) {
					Synapse* synapses = network[i][j].getSynapses();
					for(unsigned int k = 0; k < layerSizes[i+1]; ++k){
						*(pos++) = synapses[k].getWeight();
					}
				}
			}
		}
		out.write((char*)netBuffer, sizeof(double)*weights);
		delete [] netBuffer;
	}

	NeuralNetwork NeuralNetwork::deserializeFromStream(std::istream& in){
		NeuralNetwork ret;

		ret.activeFunc = Activator::deserialize(in);
		in.read((char*)&ret.layerCount, sizeof(uint));
		ret.layerSizes = new uint[ret.layerCount];
		in.read((char*)ret.layerSizes, sizeof(uint)*ret.layerCount);

		ret.allocNetwork();
		ret.initAndLink();
		
		std::cout << "Created network with " << ret.layerCount << " layers" << std::endl;

		uint weights = ret.getTotalWeights();
		double* netBuffer = new double[weights];
		double* pos = netBuffer;
		in.read((char*)netBuffer, sizeof(double) * weights);
		for(uint i = 0; i < ret.layerCount; ++i) {
			for(uint j = 0; j < ret.layerSizes[i]; ++j) {
				ret.network[i][j].setBias(*(pos++));
				if(i != ret.layerCount-1) {
					Synapse* synapses = ret.network[i][j].getSynapses();
					for(unsigned int k = 0; k < ret.layerSizes[i+1]; ++k){
						std::cout << "Layer " << i << " Neuron " << j << " output " << k << " has weight " << *pos << std::endl;
						synapses[k].setWeight(*(pos++));
					}
				}
			}
		}
		delete [] netBuffer;
		return ret;
	}
}