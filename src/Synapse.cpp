#include "Neuron.h"
#include "Synapse.h"

namespace nnet {
	void Synapse::linkTarget(Neuron& t){
		target = &t;
		t.linkInput(this);
	}

	void Synapse::feedForward(double activation) {
		activationState = activation * weight;
	}

	void Synapse::computeDerivatives() {
		dEdW = source->getOutput() * target->getdEdNet();
		dEdIn = weight * target->getdEdNet();
	}

	void Synapse::performGradientDescent(double learnRate){
		weight -= learnRate * dEdW;
	}
}