#ifndef __NEURON_H
#define __NEURON_H

namespace nnet {
	class Synapse;
	class Activator;

	class Neuron {
	private:
		double netInput;
		double activationLevel;
		double bias;
		Activator* activator;

		double dEdNet;
		double dEdOut;

		Synapse* outputs;
		unsigned int outputCount;

		Synapse** inputs;
		unsigned int inputCount;

		bool isInputLayer(){ return inputCount == 0; }
		bool isOutputLayer(){ return outputCount == 0; }
	public:
		Neuron() {}
		Neuron(Activator* activator, unsigned int inputn, unsigned int outputn) { init(activator, inputn, outputn); }
		void init(Activator* activator, unsigned int inputn, unsigned int outputn);
		~Neuron();

		void feedForward();
		void computeDerivatives();
		void trainOutputWeights(double learnRate);
		double computeError(double target);
		void linkInput(Synapse* link);
		void activate(double d) { activationLevel = d; }
		void setBias(double b) { bias = b; }

		Synapse* getSynapses() { return outputs; }
		double getOutput() { return activationLevel; }
		double getBias() { return bias; }
		double getdEdOut() { return dEdOut; }
		double getdEdNet() { return dEdNet; }
	};
}

#endif