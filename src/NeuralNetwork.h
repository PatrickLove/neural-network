#ifndef __NEURAL_NETWORK_H
#define __NEURAL_NETWORK_H

#include <iostream>
#include <algorithm>
#include <vector>
#include <cstdlib>
#include "Neuron.h"
#include "Synapse.h"
#include "Activation/Activator.h"

namespace nnet {

	typedef std::vector<double> NetworkInput;

	class NeuralNetwork
	{
	private:
		Neuron** network;
		Activator* activeFunc;
		unsigned int layerCount;
		unsigned int* layerSizes;

		void initAndLink();

		void allocNetwork();

		int outputCount() const { return layerSizes[outputLayer()]; }
		int outputLayer() const { return layerCount-1; }
		unsigned int getTotalWeights();

		NeuralNetwork() : network(NULL), activeFunc(NULL), layerSizes(NULL) {}
	public:
		NeuralNetwork(unsigned int layers, unsigned int* layerSizeArr, Activator* func);
		NeuralNetwork(const NeuralNetwork& other);
		NeuralNetwork& operator= (const NeuralNetwork& other);
		~NeuralNetwork();
		
		void initRand(double min, double max);

		void serializeToStream(std::ostream& out);
		static NeuralNetwork deserializeFromStream(std::istream& in);

		NetworkInput eval(const NetworkInput& inputs);

		void train(const NetworkInput& expectedOutputs, double learnRate);
	};

}

#endif