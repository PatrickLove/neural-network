#include "Neuron.h"
#include "Synapse.h"
#include "Activation/Activator.h"
#include <cstddef>

namespace nnet {
	void Neuron::init(Activator* activate, unsigned int inputn, unsigned int outputn) {
		activator = activate;

		outputCount = outputn;
		outputs = outputCount ? new Synapse[outputCount] : NULL;
		for(unsigned int i = 0; i < outputCount; ++i){
			outputs[i].linkSource(this);
		}

		inputCount = 0;
		inputs = inputn ? new Synapse*[inputn] : NULL;
	}
	Neuron::~Neuron() {
		delete [] inputs;
		delete [] outputs;
	}

	void Neuron::linkInput(Synapse* link) {
		inputs[inputCount++] = link;
	}

	double Neuron::computeError(double target){
		dEdOut = activationLevel - target;
		return (dEdOut*dEdOut) / 2;
	}
	void Neuron::computeDerivatives(){
		if(!isOutputLayer()) { //If it is it will have been computed in computeError(double)
			dEdOut = 0;
			for (unsigned int i = 0 ; i < outputCount; ++i){
				dEdOut += outputs[i].getdEdIn();
			}
		}
		dEdNet = dEdOut * activator->ddNet(netInput);
		for(unsigned int i = 0; i < inputCount; ++i){
			inputs[i]->computeDerivatives();
		}
	}
	void Neuron::trainOutputWeights(double learnRate) {
		for (unsigned int i  = 0; i < outputCount; ++i){
			outputs[i].performGradientDescent(learnRate);
		}
		bias -= learnRate * dEdNet;
	}
	void Neuron::feedForward(){
		if(!isInputLayer()) {
			netInput = bias;
			for (unsigned int i  = 0; i < inputCount; ++i){
				netInput += inputs[i]->getActivation();
			}
			activationLevel = (*activator)(netInput);
		}
		for (unsigned int i  = 0; i < outputCount; ++i){
			outputs[i].feedForward(activationLevel);
		}
	}
}