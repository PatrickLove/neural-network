#include <vector>
#include <algorithm>
#include <iostream>
#include "NeuralNetwork.h"

namespace nnet {

	struct TrainingItem
	{
		NetworkInput input;
		NetworkInput expected;
		TrainingItem(NetworkInput in, NetworkInput out) : input(in), expected(out) {}
	};

	typedef std::vector<TrainingItem> TrainingSet;

	class NetworkTrainer {

		const TrainingSet& trainSet;
		double learningRate;

	public:
		NetworkTrainer(const TrainingSet& data, double learnRate) : trainSet(data), learningRate(learnRate) {}

		void trainNetworkOnce(NeuralNetwork& network) {
			for (unsigned int i = 0; i < trainSet.size(); ++i)
			{
				const TrainingItem& item = trainSet[i];
				network.eval(item.input);
				network.train(item.expected, learningRate);
			}
		}

	};
}