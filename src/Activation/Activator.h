#ifndef __ACTIVATOR_H
#define __ACTIVATOR_H

#include <iostream>

namespace nnet {
	class Activator {
	public:
		virtual ~Activator();

		virtual double operator() (double net) = 0;
		virtual double ddNet(double net) = 0;
		virtual void serialize(std::ostream& out) = 0;
		virtual Activator* clone() = 0;
		static Activator* deserialize(std::istream& in);
	};
}

#endif