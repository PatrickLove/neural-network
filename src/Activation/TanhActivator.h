#include "Activator.h"
#include <cmath>

#define TANH_ACTIVATOR_TYPE '\2'

namespace nnet {
	class TanhActivator : public Activator {
		double operator() (double net) {
			return tanh(net);
		}
		double ddNet(double net) {
			double sech = 1/cosh(net);
			return sech*sech;
		}
		void serialize(std::ostream& out){
			out.put(TANH_ACTIVATOR_TYPE);
		}
		TanhActivator* clone() { return new TanhActivator(); }
	};
}