#include "Activator.h"
#include <cmath>

#define ATAN_ACTIVATOR_TYPE '\1'

namespace nnet {
	class AtanActivator : public Activator {
		double operator() (double net) {
			return atan(net);
		}
		double ddNet(double net) {
			return 1 / (net*net+1);
		}
		void serialize(std::ostream& out) {
			out.put(ATAN_ACTIVATOR_TYPE);
		}
		AtanActivator* clone() { return new AtanActivator(); }
	};
}