#include "Activator.h"
#include "LinearActivator.h"
#include "AtanActivator.h"
#include "TanhActivator.h"
#include "SoftSignActivator.h"

namespace nnet {

	Activator::~Activator() {}

	Activator* Activator::deserialize(std::istream& in){
		char type;
		in.get(type);
		switch(type) {
		case LINEAR_ACTIVATOR_TYPE:
			double m;
			in.read((char*)&m, sizeof(double));
			return new LinearActivator(m);
		case ATAN_ACTIVATOR_TYPE:
			return new AtanActivator();
		case TANH_ACTIVATOR_TYPE:
			return new TanhActivator();
		case SOFTSIGN_ACTIVATOR_TYPE:
			return new SoftSignActivator();
		}
	};
}