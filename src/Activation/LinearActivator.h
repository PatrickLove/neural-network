#include "Activator.h"
#define LINEAR_ACTIVATOR_TYPE '\0'

namespace nnet {
	class LinearActivator : public Activator {
	private:
		double slope;
	public:
		LinearActivator() : slope(1.0) {};
		LinearActivator(double m) : slope(m) {}
		double operator() (double net) {
			return slope * net;
		}
		double ddNet(double net) {
			return slope;
		}
		void serialize(std::ostream& out){
			out.put(LINEAR_ACTIVATOR_TYPE);
			out.write((char*)&slope, sizeof(double));
		}
		LinearActivator* clone() { return new LinearActivator(slope); }
	};
}