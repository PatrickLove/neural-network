#include "Activator.h"
#include <cmath>

#define SOFTSIGN_ACTIVATOR_TYPE '\3'

namespace nnet {
	class SoftSignActivator : public Activator {
	public:
		double operator() (double net) {
			return net / (1 + fabs(net));
		}
		double ddNet(double net) {
			double recip = 1 / (1 + fabs(net));
			return recip*recip;
		}
		void serialize(std::ostream& out){
			out.put(SOFTSIGN_ACTIVATOR_TYPE);
		}
		SoftSignActivator* clone() { return new SoftSignActivator(); }
	};
}